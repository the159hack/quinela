@extends('back.layouts.plantilla')
@section('titulo','Retagol Usuarios')
@section('content')
<div class="container">
    <h2 class="float-left">Lista de usuarios</h2>
    @isset($permisos['usuarios_agregar'])
        <a href="{{ asset('usuarios/agregar') }}"> <button type="button" class="btn btn-success float-right">Agregar</button></a>
    @endisset
    <table class="table table-hover">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Usuario</th>
                <th scope="col">Nombres</th>
                <th scope="col">Celular</th>
                <th scope="col">Grupo</th>
                <th scope="col">Activo</th>
                <th scope="col">Eliminado</th>
                <th scope="col" class="text-center">Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach($usuarios as $key => $usuario)
            <tr>
                <th scope="row">{{ $usuario->id }}</th>
                <td>{{ $usuario->usuario }}</td>
                <td>{{ $usuario->nombres.' '.$usuario->apellido_paterno.' '.$usuario->apellido_materno }}</td>
                <td>{{ $usuario->celular }}</td>
                <td>{{ $grupos[$usuario->grupos_id] }}</td>
                <td class="status_{{ $usuario->activo }}"></a>{{ $usuario->activo == 1 ? 'ACTIVO' : 'INACTIVO'}}</td>
                <td class=""></a>{{ $usuario->eliminado == 1 ? 'ELIMINADO' : ''}}</td>
                <td>
                    @isset($permisos['usuarios_editar'])
                        <a href="{{ route('usuarios.editar', $usuario->id) }}"><button type="button" class="btn btn-sm btn-outline-primary float-right">Editar</button></a>    
                    @endisset
                    @if(!empty($permisos['usuarios_eliminar']) && !$usuario->eliminado)
                        <a href="{{ route('usuarios.eliminar', $usuario->id) }}"><button type="button" class="btn btn-sm btn-outline-danger float-right">Eliminar</button></a>
                    @endif
                    @if(!empty($permisos['usuarios_recuperar']) && $usuario->eliminado)
                        <a href="{{ route('usuarios.recuperar', $usuario->id) }}"><button type="button" class="btn btn-sm btn-outline-success float-right">Recuperar</button></a>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{ $usuarios->links() }}
</div>

@endsection