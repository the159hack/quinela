@extends('back.layouts.plantilla')

@section('content')
<form action="{{ route('usuarios.actualizar', $usuario->id) }}" method="POST">
    @method('put')
    @csrf
    <div class="container mb-3">
        <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top border bg-dark nav_usuario">
            <button type="submit" class="btn btn-primary ml-auto float-right">Guardar</button>
        </nav>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="usuario">Usuario</label><span class="required">*</span>
                    @error('usuario') <span class="spinner-grow text-danger"></span> @enderror
                    <input type="text" class="form-control" name="usuario" value="{{ $usuario->usuario }}" required>
                </div>
                <div class="form-group">
                    <label for="nombres">Nombre (s)</label><span class="required">*</span>
                    @error('nombres') <span class="spinner-grow text-danger"></span> @enderror
                    <input type="text" class="form-control" name="nombres" value="{{ $usuario->nombres }}" required>
                </div>
                <div class="form-group">
                    <label for="apellido_paterno">Apellido paterno</label><span class="required">*</span>
                    @error('apellido_paterno') <span class="spinner-grow text-danger"></span> @enderror
                    <input type="text" class="form-control" name="apellido_paterno" value="{{ $usuario->apellido_paterno }}" required>
                </div>
                <div class="form-group">
                    <label for="apellido_materno">Apellido materno</label><span class="required">*</span>
                    @error('apellido_materno') <span class="spinner-grow text-danger"></span> @enderror
                    <input type="text" class="form-control" name="apellido_materno" value="{{ $usuario->apellido_materno }}" required>
                </div>
                <div class="form-group">
                    <label for="email">Email</label><span class="required">*</span>
                    @error('email') <span class="spinner-grow text-danger"></span> @enderror
                    <input type="email" class="form-control" name="email" value="{{ $usuario->email }}" required>
                </div>
                <div class="form-group">
                    <label for="telefono">Telefono</label>
                    @error('telefono') <span class="spinner-grow text-danger"></span> @enderror
                    <input type="text" class="form-control" name="telefono" value="{{ $usuario->telefono }}">
                </div>
                <div class="form-group">
                    <label for="celular">N&uacute;mero de celular</label><span class="required">*</span>
                    @error('celular') <span class="spinner-grow text-danger"></span> @enderror
                    <input type="text" class="form-control" name="celular" value="{{ $usuario->celular }}" required>
                </div>
                <div class="form-check">
                    <label class="form-check-label" for="activo">Activo</label>
                    <input type="hidden" name="activo" value="0"/>
                    <input type="checkbox" name="activo" value="1" {{ $usuario->activo ? 'checked' : '' }} />
                </div>
            </div>
            <div class="col-sm-6">
                <label class="form-check-label" for="grupos_id">Grupo</label><span class="required">*</span>
                @error('grupos_id') <span class="spinner-grow text-danger"></span> @enderror
                <select id="inputState" name="grupos_id" class="form-control" required>
                    <option></option>
                    @foreach($grupos as $grupo)
                        <option value="{{ $grupo->id }} " {{ ($usuario->grupos_id == $grupo->id) ? 'selected' : ''  }} >{{ $grupo->nombre }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</form>
@endsection