@extends('back.layouts.plantilla')

@section('content')
<div class="container">
    <form action="{{ route('grupos.actualizar', $datos['grupo']->id) }}" method="POST">
        @method('put')
        @csrf
        <div class="container mb-3">
            <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top border bg-dark nav_usuario">
                <button type="submit" class="btn btn-primary ml-auto float-right">Guardar</button>
            </nav>
        </div>
        <div class="row">
            <div class="col-sm-4">
                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control" name="nombre" value="{{ $datos['grupo']->nombre }}" required>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label" for="activo">Activo</label>
                        <input type="hidden" name="activo" value="0"/>
                        <input type="checkbox" name="activo" value="1" {{ $datos['grupo']->activo ? 'checked' : '' }} />
                    </div>  
            </div>
            <div class="col-sm-8">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">Activo</th>
                            <th scope="col">Funcion</th>
                            <th scope="col">Descripción</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($datos['funciones'] as $funcion)
                            <tr>
                                <td>
                                    <input type="checkbox" name="permisos_id[]" value="{{ $funcion->id }}" {{ in_array($funcion->id, $datos['permisos']) ? 'checked' : '' }} />
                                </td>
                                <td>{{ $funcion->funcion }}</td>
                                <td>{{ $funcion->descripcion }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </form>
</div>
@endsection