@extends('back.layouts.plantilla')
@section('titulo','Retagol Grupos')
@section('content')
<div class="container">
    <h2 class="float-left">Permisos</h2>
    @isset($permisos['grupos_agregar'])
        <a href="{{ asset('grupos/agregar') }}"> <button type="button" class="btn btn-success float-right">Agregar</button></a>
    @endisset
    <table class="table table-hover">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Nombre</th>
                <th scope="col">Activo</th>
                <th scope="col">Eliminado</th>
                <th scope="col" class="text-center">Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach($grupos as $key => $grupo)
            <tr>
                <th scope="row">{{ $grupo->id }}</th>
                <td>{{ $grupo->nombre }}</td>
                <td class="status_{{ $grupo->activo }}"></a>{{ $grupo->activo == 1 ? 'ACTIVO' : 'INACTIVO'}}</td>
                <td class=""></a>{{ $grupo->eliminado == 1 ? 'ELIMINADO' : ''}}</td>
                <td>
                    @isset($permisos['grupos_editar'])
                        <a href="{{ route('grupos.editar', $grupo->id) }}"><button type="button" class="btn btn-sm btn-outline-primary float-right">Editar</button></a> 
                    @endisset
                    @if(!empty($permisos['grupos_eliminar']) && !$grupo->eliminado)
                        <a href="{{ route('grupos.eliminar', $grupo->id) }}"><button type="button" class="btn btn-sm btn-outline-danger float-right">Eliminar</button></a>
                    @endif
                    @if(!empty($permisos['grupos_recuperar']) && $grupo->eliminado)
                        <a href="{{ route('grupos.recuperar', $grupo->id) }}"><button type="button" class="btn btn-sm btn-outline-success float-right">Recuperar</button></a>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{ $grupos->links() }}
</div>
@endsection