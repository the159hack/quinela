@extends('back.layouts.plantilla')

@section('content')
<form class="mt-5" action="{{ route('grupos.guardar') }}" method="POST">
    @csrf
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top border bg-dark nav_usuario">
            <button type="submit" class="btn btn-primary ml-auto float-right">Guardar</button>
        </nav>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="nombre">Nombre</label>
                    <input type="text" class="form-control" name="nombre" value="{{ old('nombre') }}" required>
                </div>
               
                <div class="form-check">
                    <label class="form-check-label" for="activo">Activo</label>
                    <input type="hidden" name="activo" value="0"/>
                    <input type="checkbox" name="activo" value="1" {{ old('activo') ? 'checked' : '' }} />
                </div>
            </div>
            <div class="col-sm-8">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">Activo</th>
                            <th scope="col">Funcion</th>
                            <th scope="col">Descripción</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($datos['funciones'] as $funcion)
                        <tr>
                            <td>
                                <input type="checkbox" name="permisos_id[]" value="{{ $funcion->id }}" />
                            </td>
                            <td>{{ $funcion->funcion }}</td>
                            <td>{{ $funcion->descripcion }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</form>
@endsection