<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
@include('back.layouts.head')

</head>

<body class="hold-transition sidebar-mini layout-fixed body-color">
    <div id="app">
        <div class="wrapper">
            <!-- Navbar -->
            @include('back.layouts.nav_notificaciones')
            <!-- Navbar -->
            <!-- Main Sidebar Container -->
            @include('back.layouts.sidebar')
            <!-- Main Sidebar Container -->

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper pt-5 pb-5">
                <!-- /.Alerta -->
                @if (session('datos'))
                    <div class="alert {{ session('datos')['tipo_alerta'] }}">
                        {{ session('datos')['mensaje'] }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span arial-hidden="true">&times;</span>
                        </button>
                    </div>

                @endif
                <!-- Main content -->
                <section class="content pt-4">
                    @yield('content')
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <!-- /.footer -->
            @include('back.layouts.footer')
            <!-- /.footer -->
        </div>
    </div>
</body>

</html>


