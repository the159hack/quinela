@yield('head')
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>@yield('titulo')</title>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
<script src="{{ asset('dist/js/adminlte.js') }}"></script>

<!-- Font Awesome Icons -->
<link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">

<!-- Fonts -->
<link rel="dns-prefetch" href="//fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

<!-- Styles -->
<link href="{{ asset('dist/css/adminlte.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link href="{{ asset('css/retagol.css') }}" rel="stylesheet">
<link href="{{ asset('css/usuarios.css') }}" rel="stylesheet"> {{-- ESTE SE TIENE QUE MOVER A SU RESPECTIVO ARCHIVO --}}
{{-- TODO AGREGAR SECCION DONDE SE CARGUEN LOS ESTILOS DEPENDIENDO EL MODULO CON BLADE --}}