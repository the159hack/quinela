<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('index');

Route::get('usuarios', 'UsuariosController@usuarios_listar');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// USUARIOS
Route::get('usuarios', 'usuarios\UsuarioController@usuarios_listar')->name('usuarios');
Route::get('usuarios/mostrar/{id}', 'usuarios\UsuarioController@usuario_mostrar')->name('usuarios.mostrar')->where('id', '[0-9]+');
Route::get('usuarios/editar/{id}', 'usuarios\UsuarioController@usuario_editar')->name('usuarios.editar')->where('id', '[0-9]+');
Route::get('usuarios/agregar', 'usuarios\UsuarioController@usuario_agregar')->name('usuarios.agregar');
Route::post('usuarios/guardar', 'usuarios\UsuarioController@usuario_guardar')->name('usuarios.guardar');
Route::put('usuarios/actualizar/{id}', 'usuarios\UsuarioController@usuario_actualizar')->name('usuarios.actualizar')->where('id', '[0-9]+');
Route::get('usuarios/eliminar/{id}', 'usuarios\UsuarioController@usuario_eliminar')->name('usuarios.eliminar')->where('id', '[0-9]+');
Route::get('usuarios/recuperar/{id}', 'usuarios\UsuarioController@usuario_recuperar')->name('usuarios.recuperar')->where('id', '[0-9]+');
// FIN USUARIOS

//GRUPOS
Route::get('grupos', 'grupos\GrupoController@grupos_listar')->name('grupos');
Route::get('grupos/editar/{id}', 'grupos\GrupoController@grupo_editar')->name('grupos.editar')->where('id', '[0-9]+');
Route::get('grupos/agregar', 'grupos\GrupoController@grupo_agregar')->name('grupos.agregar');
Route::post('grupos/guardar', 'grupos\GrupoController@grupo_guardar')->name('grupos.guardar');
Route::get('grupos/eliminar/{id}', 'grupos\GrupoController@grupo_eliminar')->name('grupos.eliminar')->where('id', '[0-9]+');
Route::get('grupos/recuperar/{id}', 'grupos\GrupoController@grupo_recuperar')->name('grupos.recuperar')->where('id', '[0-9]+');
Route::put('grupos/actualizar/{id}', 'grupos\GrupoController@grupo_actualizar')->name('grupos.actualizar')->where('id', '[0-9]+');

// FIN GRUPOS

