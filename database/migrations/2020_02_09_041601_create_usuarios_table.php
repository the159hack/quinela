<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     * Crea la tabla de usuarios
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('usuario', 50)->unique()->collation('utf8_spanish2_ci');
            $table->string('nombres', 100)->collation('utf8_spanish2_ci');
            $table->string('apellido_paterno', 100)->collation('utf8_spanish2_ci');
            $table->string('apellido_materno', 100)->collation('utf8_spanish2_ci');
            $table->string('email')->unique()->collation('utf8_spanish2_ci');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->string('telefono', 10);
            $table->string('celular', 10);
            $table->tinyInteger('activo')->default(0);
            $table->Integer('grupos_id');
            $table->tinyInteger('eliminado')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     * Elimina la tabla de usuarios si existe
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
