<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdministradoresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('usuarios')->insert([
            'usuario' => 'Cuco1441',
            'nombres' => 'Jose Refugio',
            'apellido_paterno' => 'Garcia',
            'apellido_materno' => 'Rodriguez',
            'email' => 'cuco1441@gmail.com',
            'email_verified_at' => '2010/02/14',
            'password' => 'Refugio1441',  
            'telefono' => '3471048138',
            'celular' => '3471048138',
            'activo' => '1',
            'grupos_id' => 1
        ]);
        DB::table('usuarios')->insert([
            'usuario' => 'MartinLr',
            'nombres' => 'Martin',
            'apellido_paterno' => 'Lopez',
            'apellido_materno' => 'Ramirez',
            'email' => 'MartinLR@gmail.com',
            'email_verified_at' => '2010/02/14',
            'password' => 'the150hack',  
            'telefono' => '3471001122',
            'celular' => '3471012233',
            'activo' => '1',
            'grupos_id' => 1
        ]);
    }
}
