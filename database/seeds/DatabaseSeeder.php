<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(AdministradoresSeeder::class);
        $this->call(FuncionesSeeder::class);
        $this->call(GruposSeeder::class);
        $this->call(PermisosSeeder::class);
    }
}
