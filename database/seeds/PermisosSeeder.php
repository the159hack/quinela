<?php

use Illuminate\Database\Seeder;

class PermisosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permisos')->insert([
            'grupos_id' => 1,
            'funciones_id' => 1,
        ]);
        DB::table('permisos')->insert([
            'grupos_id' => 1,
            'funciones_id' => 2,
        ]);
        DB::table('permisos')->insert([
            'grupos_id' => 1,
            'funciones_id' => 3,
        ]);
        DB::table('permisos')->insert([
            'grupos_id' => 1,
            'funciones_id' => 4,
        ]);
        DB::table('permisos')->insert([
            'grupos_id' => 1,
            'funciones_id' => 5,
        ]);
        DB::table('permisos')->insert([
            'grupos_id' => 1,
            'funciones_id' => 6,
        ]);
        DB::table('permisos')->insert([
            'grupos_id' => 1,
            'funciones_id' => 7,
        ]);
        DB::table('permisos')->insert([
            'grupos_id' => 1,
            'funciones_id' => 8,
        ]);
        DB::table('permisos')->insert([
            'grupos_id' => 1,
            'funciones_id' => 9,
        ]);
        DB::table('permisos')->insert([
            'grupos_id' => 1,
            'funciones_id' => 10,
        ]);
        DB::table('permisos')->insert([
            'grupos_id' => 1,
            'funciones_id' => 11,
        ]);
        DB::table('permisos')->insert([
            'grupos_id' => 1,
            'funciones_id' => 12,
        ]);
    }
}
