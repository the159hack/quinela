<?php

use Illuminate\Database\Seeder;

class GruposSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('grupos')->insert([
            'id' => 1,
            'nombre' => 'Retagol',
            'activo' => 1,
            'eliminado' => 0
        ]);
        DB::table('grupos')->insert([
            'nombre' => 'Administrador',
            'activo' => 1,
            'eliminado' => 0
        ]);
    }
}
