<?php

use Illuminate\Database\Seeder;

class FuncionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('funciones')->insert([
            'funcion' => 'usuarios_editar',
            'descripcion' => 'Permite editar el usuario',
            'categorias_id' => 1
        ]);
        DB::table('funciones')->insert([
            'funcion' => 'usuarios_eliminar',
            'descripcion' => 'Permite eliminar usuarios',
            'categorias_id' => 1
        ]);
        DB::table('funciones')->insert([
            'funcion' => 'usuarios_recuperar',
            'descripcion' => 'Permite recuperar los usuarios que ya fueron eliminados',
            'categorias_id' => 1
        ]);
        DB::table('funciones')->insert([
            'funcion'=>'usuarios_eliminados_ver',
            'descripcion' => 'Permite ver los usuarios que ya han sido eliminados',
            'categorias_id' => 1
        ]);
        DB::table('funciones')->insert([
            'funcion'=>'usuarios_agregar',
            'descripcion' => 'Permite agregar nuevos usuarios',
            'categorias_id' => 1
        ]);
        DB::table('funciones')->insert([
            'funcion'=>'usuarios_ver',
            'descripcion' => 'Permite ver la lista de usuarios',
            'categorias_id' => 1
        ]);
        DB::table('funciones')->insert([
            'funcion' => 'grupos_editar',
            'descripcion' => 'Permite editar el usuario',
            'categorias_id' => 2
        ]);
        DB::table('funciones')->insert([
            'funcion' => 'grupos_eliminar',
            'descripcion' => 'Permite eliminar grupos',
            'categorias_id' => 2
        ]);
        DB::table('funciones')->insert([
            'funcion' => 'grupos_recuperar',
            'descripcion' => 'Permite recuperar los grupos que ya fueron eliminados',
            'categorias_id' => 2
        ]);
        DB::table('funciones')->insert([
            'funcion'=>'grupos_eliminados_ver',
            'descripcion' => 'Permite ver los grupos que ya han sido eliminados',
            'categorias_id' => 2
        ]);
        DB::table('funciones')->insert([
            'funcion'=>'grupos_agregar',
            'descripcion' => 'Permite agregar nuevos grupos',
            'categorias_id' => 2
        ]);
        DB::table('funciones')->insert([
            'funcion'=>'grupos_ver',
            'descripcion' => 'Permite ver la lista de grupos',
            'categorias_id' => 2
        ]);
    }
}
