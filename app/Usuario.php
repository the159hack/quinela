<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Traits\Encryptable;

class Usuario extends Model
{
    use Notifiable;

    /**
     * Los atributos que se asignables en masa.
     *
     * @var array
     */
    protected $fillable = [
        'usuario', 
        'nombres', 
        'apellido_paterno', 
        'apellido_materno', 
        'email', 
        'password',
        'telefono',
        'celular',
        'grupos_id',
        'activo',
    ];

    /**
     *Atributos que deben de estar ocultos.
     *
     * @var array
     */
    protected $hidden = [
        'password', 
        'remember_token',
    ];
    /**
     * Los atributos que deben convertirse a tipos nativos.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
