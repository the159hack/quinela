<?php

namespace App\Http\Controllers\usuarios;

use App\Http\Controllers\Controller;
use App\Http\Requests\UsuarioRequest;
use App\Usuario;
use App\Base;

class UsuarioController extends Controller
{
    // Nómbre del modelo
    var $usuario = 'Usuario';


    /**
     * Muestra la lista de usuarios.
     *
     * @return \Illuminate\Http\Response
     */
    public function usuarios_listar()
    {
        $base = new Base();
        if(empty($base->tiene_permiso(['usuarios_ver']))) {
            return redirect()->route('index');
        }

        $permisos = $base->tiene_permiso(['usuarios_editar', 'usuarios_recuperar', 'usuarios_eliminar', 'usuarios_agregar']);
        $usuarios = Usuario::paginate(20);
        $grupos = $base->datos_get(['id','nombre'], 'grupos', ['activo' =>  1]);
        foreach($grupos as $grupo){
            $gr[$grupo->id] = $grupo->nombre;
        }

        return view('back.usuarios.index', ['usuarios' => $usuarios, 'grupos' => $gr, 'permisos' => $permisos]);
    }

    /**
     * Muestra un usuario especifico.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function usuario_mostrar($id)
    {
        $usuario = Usuario::findOrFail($id);
        dd($usuario);
    }

    /**
     * Muestra el formulario para editar un usuario.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function usuario_editar($id)
    {
        $base = new Base();
        if(empty($base->tiene_permiso(['usuarios_editar']))) {
            return redirect()->route('index');
        }
        
        $grupos = $base->datos_get(['id','nombre'], 'grupos', ['activo' =>  1]);
        $usuario = Usuario::findOrFail($id);
        return view('back.usuarios.editar',['usuario' => $usuario, 'grupos' => $grupos]);
    }

    /**
     * Muestra el formulario para crear un nuevo usuario.
     *
     * @return \Illuminate\Http\Response
     */
    public function usuario_agregar()
    {
        $base = new Base();
        if(empty($base->tiene_permiso(['usuarios_agregar']))) {
            return redirect()->route('index');
        }
        
        $grupos = $base->datos_get(['id','nombre'], 'grupos', ['activo' =>  1]);
        return view('back.usuarios.agregar', ['grupos' => $grupos]);
    }

    /**
     * Guarda un usuario.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function usuario_guardar(UsuarioRequest $request)
    {
        $base = new Base();
        if(empty($base->tiene_permiso(['usuarios_agregar']))) {
            return redirect()->route('index');
        }
        
        $usuario = new Usuario();
        $usuario->usuario = $request->get('usuario');
        $usuario->nombres = $request->get('nombres');
        $usuario->apellido_paterno = $request->get('apellido_paterno');
        $usuario->apellido_materno = $request->get('apellido_materno');
        $usuario->email = $request->get('email');
        $usuario->password = bcrypt($request->get('password'));
        $usuario->telefono = $request->get('telefono');
        $usuario->celular = $request->get('celular');
        $usuario->activo = $request->activo;
        $usuario->grupos_id = $request->grupos_id;
        $res = $usuario->save();
        $mensaje = $base->mensajes($res, 'guardar', $this->usuario);
        return redirect('usuarios')->with('datos', $mensaje);
    }

    /**
     * Actualiza un usuario.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function usuario_actualizar(UsuarioRequest $request, $id)
    {
        $base = new Base();
        if(empty($base->tiene_permiso(['usuarios_editar']))) {
            return redirect()->route('index');
        }

        $usuario = Usuario::findOrFail($id);
        $usuario->usuario = $request->get('usuario');
        $usuario->nombres = $request->get('nombres');
        $usuario->apellido_paterno = $request->get('apellido_paterno');
        $usuario->apellido_materno = $request->get('apellido_materno');
        $usuario->email = $request->get('email');
        $usuario->telefono = $request->get('telefono');
        $usuario->celular = $request->get('celular');
        $usuario->activo = $request->activo;
        $usuario->grupos_id = $request->grupos_id;
        $res = $usuario->update();;
        $mensaje = $base->mensajes($res, 'actualizar', $this->usuario);
        return redirect('usuarios')->with('datos', $mensaje); 
    }

    /**
     * Elimina un usuario.
     *
     * @param  int  $id Id del usuario a eliminar
     * @return \Illuminate\Http\Response
     */
    public function usuario_eliminar($id)
    {
        $base = new Base();
        if(empty($base->tiene_permiso(['usuarios_eliminar']))) {
            return redirect()->route('index');
        }
        
        $usuario = new Usuario;
        $datos = [
            'eliminado' => 1, 
            'activo' => 0, 

        ];
        $res = $usuario::where('id', $id)->update($datos);
        $mensaje = $base->mensajes($res, 'eliminar', $this->usuario);
        return redirect('usuarios')->with('datos', $mensaje);
    }
    /**
     * Recupera un usuario eliminado.
     *
     * @param  int  $id Id del usuario a recuoerar
     * @return \Illuminate\Http\Response
     */
    public function usuario_recuperar($id)
    {
        $base = new Base();
        if(empty($base->tiene_permiso(['usuarios_recuperar']))) {
            return redirect()->route('index');
        }
        
        $res = Usuario::where('id', $id)->update(['eliminado' => 0]);
        $mensaje = $base->mensajes($res, 'recuperado', $this->usuario);
        return redirect('usuarios')->with('datos', $mensaje);
    }
}
