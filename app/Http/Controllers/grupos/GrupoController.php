<?php

namespace App\Http\Controllers\grupos;

use App\Http\Controllers\Controller;
use App\Grupo;
use App\Http\Requests\GrupoRequest;
use DB;
use App\Base;

class GrupoController extends Controller
{

    // Nómbre del modelo
    var $grupo = 'Grupo';

    /**
     * Muestra la lista de grupos.
     *
     * @return \Illuminate\Http\Response
     */
    public function grupos_listar()
    {
        $base = new Base();
        if (empty($base->tiene_permiso(['grupos_ver']))) {
            return redirect()->route('index');
        }
        
        $permisos = $base->tiene_permiso(['grupos_ver', 'grupos_editar', 'grupos_recuperar', 'grupos_eliminar', 'grupos_agregar']);
        $grupos = Grupo::paginate(20);
        return view('back.grupos.index', ['grupos' => $grupos, 'permisos' => $permisos]);
    }

    /**
     * Muestra el formulario para editar un grupo.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function grupo_editar($id)
    {
        $base = new Base();
        if (empty($base->tiene_permiso(['grupos_editar']))) {
            return redirect()->route('index');
        }

        $datos['grupo'] = Grupo::findOrFail($id);
        $datos['funciones'] = DB::table('funciones')->get();
        $datos['permisos'] = DB::table('permisos as p')
        ->join('funciones as f', 'f.id', '=', 'p.funciones_id')
        ->select('f.id')
        ->where('p.grupos_id', '=', $id)
        ->get()->map->id->toArray();
        return view('back.grupos.editar', ['datos' => $datos]);
    }

     /**
     * Muestra el formulario para crear un nuevo grupo.
     *
     * @return \Illuminate\Http\Response
     */
    public function grupo_agregar()
    {
        $base = new Base();
        if (empty($base->tiene_permiso(['grupos_agregar']))) {
            return redirect()->route('index');
        }
        $datos['funciones'] = DB::table('funciones')->get();
        return view('back.grupos.agregar', ['datos' => $datos]);
    }

    /**
     * Guarda un grupo.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function grupo_guardar(GrupoRequest $request)
    {
        $base = new Base();
        if (empty($base->tiene_permiso(['grupos_agregar']))) {
            return redirect()->route('index');
        }

        $grupo = new Grupo();
        $grupo->nombre = $request->nombre;
        $grupo->activo = $request->activo;
        $grupo->modificado_por = 1; // TODO ESTO SE SACA DE LA SESSION
        $res = $grupo->save();
        $mensaje = $base->mensajes($res, 'guardar', $this->grupo);

        if($res){
            $permisos = isset($request->permisos_id) ? $request->permisos_id : array();
            $base->permisos_guardar($grupo->id, $permisos);
        }
        return redirect('grupos')->with('datos', $mensaje);
    }

    /**
     * Actualiza un grupo.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function grupo_actualizar(GrupoRequest $request, $id)
    {
        $base = new Base();
        if (empty($base->tiene_permiso(['grupos_editar']))) {
            return redirect()->route('index');
        }

        $grupo = Grupo::findOrFail($id);
        $grupo->nombre = $request->nombre;
        $grupo->activo = $request->activo;
        $grupo->modificado_por = 1; // TODO ESTO SE SACA DE LA SESSION
        $res = $grupo->update();
        $permisos = isset($request->permisos_id) ? $request->permisos_id : array();
        $base->permisos_guardar($id, $permisos);
        $mensaje = $base->mensajes($res, 'actualizar', $this->grupo);
        return redirect('grupos')->with('datos', $mensaje); 
    }

    /**
     * Elimina un grupo.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function grupo_eliminar($id)
    {
        $base = new Base();
        if (empty($base->tiene_permiso(['grupos_eliminar']))) {
            return redirect()->route('index');
        }

        $grupo = new Grupo;
        $datos = [
            'eliminado' => 1
        ];
        $res = $grupo::where('id', $id)->update($datos);
        $mensaje = $base->mensajes($res, 'eliminar', $this->grupo);
        return redirect('grupos')->with('datos', $mensaje);
    }

    /**
     * Recupera un grupo eliminado.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function grupo_recuperar($id)
    {
        $base = new Base();
        if (empty($base->tiene_permiso(['grupos_recuperar']))) {
            return redirect()->route('index');
        }
        $res = Grupo::where('id', $id)->update(['eliminado' => 0]);
        $mensaje = $base->mensajes($res, 'recuperado', $this->grupo);
        return redirect('grupos')->with('datos', $mensaje);
    }
}
