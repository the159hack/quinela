<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Base extends Model
{
    function datos_get($campos, $tabla, $condiciones = array()){
        $res = DB::table($tabla)
        ->select($campos)
        ->where($condiciones)
        ->get();
        return $res;
    }

    function guarda($tabla, $datos){
        
    }
    function tiene_permiso($funciones, $usuarios_id = 2){ 
        $permisos = DB::table('permisos as p')
        ->join('funciones as f', 'f.id', '=', 'p.funciones_id')
        ->join('usuarios as u', 'u.grupos_id', '=', 'p.grupos_id')
        ->select('f.funcion', 'f.id')
        ->where('u.id', $usuarios_id)
        ->whereIn('f.funcion', $funciones)
        ->get()->toArray();
        $res = array();
        if(!empty($permisos)){
            foreach($permisos as $p)
                $res[$p->funcion] = $p->id;
        }
        return $res;
    }

    function permisos_guardar($grupos_id, $funciones = array()){

        //Se eliminan los registros anteriores
        DB::table('permisos')->where('grupos_id', '=', $grupos_id)->delete();
        $datos = array();
        foreach($funciones as $key => $funcion) {
            $datos[$key]['funciones_id'] = $funcion;
            $datos[$key]['grupos_id'] = $grupos_id;
        }
        // Se agregan los nuevos registros
        return DB::table('permisos')->insert($datos);
    }

    function mensajes($res, $accion_tipo = false, $registro = false){
        if ($res) {
            if($accion_tipo && $registro){
                switch($accion_tipo){
                    case ('guardar'):
                        $respuesta['mensaje'] = $registro.' guardado correctamente.';
                        $respuesta['tipo_alerta'] = 'alert-success';
                        break;
                    case ('actualizar'):
                        $respuesta['mensaje'] = $registro.' actualizado correctamente.';
                        $respuesta['tipo_alerta'] = 'alert-success';
                        break;
                    case ('eliminar'):
                        $respuesta['mensaje'] = $registro.' eliminado.';
                        $respuesta['tipo_alerta'] = 'alert-danger';
                        break;
                    case ('recuperado'):
                        $respuesta['mensaje'] = $registro.' recuperado';
                        $respuesta['tipo_alerta'] = 'alert-success';
                        break;
                }
            } else{
                $respuesta['mensaje'] = 'Proceso realizado correctamente correctamente.';
                $respuesta['tipo_alerta'] = 'alert-success';
            }
        } else {
            $respuesta['mensaje'] = 'Falló, por favor verifica los datos.';
            $respuesta['tipo_alerta'] = 'alert-danger';
        }
        
        return $respuesta;
    }
}
