<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Grupo extends Model
{

    use Notifiable;
     /**
     * Los atributos que se asignables en masa.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 
        'activo',
    ];
}
